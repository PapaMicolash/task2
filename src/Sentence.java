import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * this class contains core fields, methods and regular expressions for searching, replacing, creative and deleted
 * sentence, massive words, massive marks, new sentence need for main task
 * @author Nikolay Sizykh
 * @version 2.3
 */
public class Sentence {

    /**
     * this field contains some sentence from text
     */
    String sentence;
    /**
     * this field contains regular expression for search words and marks
     */
    String partsSentence = "[A-Z][a-z]*\\S\\w|[a-z]*\\S\\w|\\S\\W";
    /**
     * contains massive of words and marks
     */
    String[] allSentence;
    /**
     * contains massive of words
     */
    String[] words;
    /**
     * contains massive of marks
     */
    String[] punctuationMarks;
    /**
     * contains new sentence from task
     */
    String newSentence;

    public Sentence(String sentence) {
        this.sentence = sentence;
    }

    public String getNewSentence() {
        return newSentence;
    }

    public void setNewSentence(String newSentence) {
        this.newSentence = newSentence;
    }

    /**
     * this method create new sentence
     */
    public void setNewSentence() {

        StringBuilder stringBuilder = new StringBuilder();
        int num = 0;
        stringBuilder.append(getWords()[num]);
        num++;
        for (int i = 0; i < getPunctuationMarks().length; i ++) {
            if (getPunctuationMarks()[i].equalsIgnoreCase(" ")) {
                    stringBuilder.append(getPunctuationMarks()[i]);
                    stringBuilder.append(getWords()[num]);
                    num++;
            } else {
                stringBuilder.append(getPunctuationMarks()[i]);

            }
        }

        this.newSentence = stringBuilder.toString();
    }

    public String getSentence() {
        return sentence;
    }

    public String[] getAllSentence() {
        return allSentence;
    }

    public String[] getWords() {
        return words;
    }

    public String[] getPunctuationMarks() {
        return punctuationMarks;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    /**
     * the method create the massive of mark and words from sentence
     */
    public void setAllSentence() {

        int num = 0;

        Pattern pattern1 = Pattern.compile(partsSentence);
        Matcher matcher1 = pattern1.matcher(sentence);

        while (matcher1.find()) {
            num++;
        }

        int lo = num;

        String[] sentenceParts = new String[lo];
        //sentence.split("[,.:;!?] | [\\s]")
        num = 0;

        Pattern pattern = Pattern.compile(partsSentence);
        Matcher matcher = pattern.matcher(sentence);

        while (matcher.find()) {
            sentenceParts[num] = matcher.group();
            num++;
        }

        allSentence = sentenceParts;
    }

    /**
     * this method create the massive of words
     */
    public void setWords() {
        String word = "\\w+";
        int numWords = 0;

        Pattern pt = Pattern.compile(word);
        Matcher mr = pt.matcher(sentence);

        while (mr.find()) {
            numWords++;
        }

        int num = 0;
        String[] words = new String[numWords];

        Pattern pattern = Pattern.compile(word);
        Matcher matcher = pattern.matcher(sentence);

        while (matcher.find()) {
            words[num] =  matcher.group();
            num++;
        }

        this.words = words;
    }

    /**
     * this method create the massive of marks
     */
    public void setPunctuationMarks() {
        String mark = "\\s|[,.;:!?]";

        int num = 0;
        String[] marks = new String[allSentence.length];

        Pattern pattern = Pattern.compile(mark);
        Matcher matcher = pattern.matcher(sentence);

        while (matcher.find()) {
            marks[num] = matcher.group();
            num++;
        }

        punctuationMarks = marks;
    }

    /**
     * this method is seaching the word in the massive of words
     * @param word which need to find
     * @return number founded word
     */
    public int checkWord(Word word) {
        int numWord = -1;

        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(word.getWord())) {
                numWord = i;
            }
        }

        return numWord;
    }

    /**
     * the method is replacing of the founded sentence on new sentence
     * @param word the word which need to replace
     * @param newWord new word
     */
    public void replaceWord(Word word,String newWord) {
        int num = checkWord(word);
        if (num > -1) {
            this.words[num] = newWord;
        }
    }

    /**
     * this method swap words
     * @param numWord1 number of the first word
     * @param numWord2 number of the last word
     */
    public void swapWords(int numWord1, int numWord2) {
        if (numWord1 > -1 && numWord1 < words.length && numWord2 > -1 && numWord2 < words.length) {
            String word1 = words[numWord1];
            String word2 = words[numWord2];

            words[numWord1] = word2;
            words[numWord2] = word1;

            setNewSentence();
        }
    }

    /**
     * this method delete sub sentence from main sentence
     * @param firstIndex
     * @param lastIndex
     */
    public void deleteSubSentence(int firstIndex, int lastIndex) {
        if (firstIndex > -1 && firstIndex < getSentence().length() &&
                lastIndex > -1 && lastIndex < getSentence().length()) {
            StringBuilder sb = new StringBuilder();
            sb.append(getSentence().substring(0, firstIndex));
            sb.append(getSentence().substring(lastIndex, getSentence().length()));
            setNewSentence(sb.toString());
        }

    }

    @Override
    public String toString() {
        return "Sentence{" +
                "sentence='" + sentence + '\'' +
                '}';
    }
}
