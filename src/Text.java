import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * class Text serves for finds strings from the text
 * @author Nikolay Sizykh
 * @version 1.7
 */

public class Text {

    /**
     * this field contains main text from file
     */
    String text;
    /**
     * this field contains regular expression for search sentences
     */
    String sentence = "([A-Z]([^?!.\\(]|\\([^\\)]*\\))*[.?!])";
    /**
     * contains number of sentences in the text
     */
    int numSentence = 0;
    /**
     * contains massive of sentences
     */
    String[] sentences;

    //[A-Z]*\s+[,:;]*+[a-z]*+[,:;]*\s+([a-z]+\s)*+([a-z]+[,:;]\s)*+([a-z]+\s)*+[a-z]+[!?.]\s

    public Text(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getNumSentence() {
        return numSentence;
    }

    public void setNumSentence(int numSentence) {
        this.numSentence = numSentence;
    }

    /**
     * this method serves for create massive of sentences
     */
    public void setSentences() {
        Pattern pattern = Pattern.compile(sentence);
        Matcher matcher = pattern.matcher(text);

        String[] strings = new String[numSentence];
        int num = 0;

        while (matcher.find()) {
            strings[num] = matcher.group();
            num++;
        }
        sentences = strings;
    }

    public String[] getSentences() {
        return sentences;
    }

    /**
     * this method serves for searching of interrogative sentences
     * @return massive of interrogative strings
     */
    public String[] findInterrogativeSentence() {
        int count = 0;
        for (int i = 0; i < sentences.length; i++) {
            Pattern pattern = Pattern.compile("[?]");
            Matcher matcher = pattern.matcher(sentences[i]);

            while(matcher.find()) {
                count++;
            }
        }

        String[] interrogativeSentences = new String[count];
        count = 0;
        for (int i = 0; i < sentences.length; i++) {
            Pattern pattern = Pattern.compile("[?]");
            Matcher matcher = pattern.matcher(sentences[i]);

            while(matcher.find()) {
                interrogativeSentences[count] = sentences[i];
                count++;
            }
        }

        return interrogativeSentences;
    }

    /**
     * this method serves for searching of Exclamatory sentences
     * @return massive of Exclamatory strings
     */
    public String[] findExclamatorySentence() {
        int count = 0;
        for (int i = 0; i < sentences.length; i++) {
            Pattern pattern = Pattern.compile("[!]");
            Matcher matcher = pattern.matcher(sentences[i]);

            while(matcher.find()) {
                count++;
            }
        }

        String[] exclamatorySentences = new String[count];
        count = 0;
        for (int i = 0; i < sentences.length; i++) {
            Pattern pattern = Pattern.compile("[!]");
            Matcher matcher = pattern.matcher(sentences[i]);

            while(matcher.find()) {
                exclamatorySentences[count] = sentences[i];
                count++;
            }
        }

        return exclamatorySentences;
    }

    /**
     * this method serves for searching of Narrative sentences
     * @return massive of Narrative strings
     */
    public String[] findNarrativeSentence() {
        int count = 0;
        for (int i = 0; i < sentences.length; i++) {
            Pattern pattern = Pattern.compile("[.]");
            Matcher matcher = pattern.matcher(sentences[i]);

            while(matcher.find()) {
                count++;
            }
        }

        String[] narrativeSentences = new String[count];
        count = 0;
        for (int i = 0; i < sentences.length; i++) {
            Pattern pattern = Pattern.compile("[.]");
            Matcher matcher = pattern.matcher(sentences[i]);

            while(matcher.find()) {
                narrativeSentences[count] = sentences[i];
                count++;
            }
        }

        return narrativeSentences;
    }

    /**
     * this method is seaching the sentence in the massive of sentences
     * @param sentence which need to find
     * @return number found sentence
     */
    public int checkSentence(Sentence sentence) {
        int num = -1;

        for (int i = 0; i < sentences.length; i++) {
            if (sentences[i].equalsIgnoreCase(sentence.getSentence())) {
                num = i;
            }
        }

        return num;
    }

    /**
     * the method is replacing of the founded sentence on new sentence
     * @param sentence the sentence which need to replace
     * @param newSentence new sentence
     */
    public void replaceSentence(Sentence sentence, String newSentence) {
        int num = checkSentence(sentence);

        if (num > -1) {
            this.sentences[num] = newSentence;
        }
    }

    @Override
    public String toString() {
        return "Text{" +
                "text='" + text + '\'' +
                '}';
    }
}
