import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * class Paper serves for reading text from file
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class Reader {

    static String filename = "src/input.txt";
    static Scanner scanner;
    int numString = 0;

    /**
     * this method finds num of strings
     * @return int number
     */
    public int getNumString() {
        return numString;
    }

    static {
        try {
            scanner = new Scanner(new FileReader(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * this method read the text from file
     * @return string which got from file
     */
    public String getNextString() {
        String string = null;
        StringBuilder sb = new StringBuilder();

        try {
            while (scanner.hasNext()) {
                string = scanner.nextLine();
                sb.append(string + " ");
                numString++;
            }
            string = sb.toString();
        } catch (Exception ex) {
        }

        return string;
    }

}
