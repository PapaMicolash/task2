/**
 * class Word serves for finds letters from the word, contains string word
 * new word and massive of letters
 * @author Nikolay Sizykh
 * @version 1.7
 */

public class Word {

    /**
     * this field contains word from sentence
     */
    String word;
    /**
     * contains new word for task
     */
    String newWord;
    /**
     * contains massive of letters
     */
    char[] letters;

    public Word(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public String getNewWord() {
        return newWord;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public char[] getLetters() {
        return letters;
    }

    public void setLetters() {
        letters = word.toCharArray();
    }

    /**
     * this method deletes the subsequent occurence of the first letter in the word
     * and create new word for replacing in the main sentence
     */
    public void eraseNextFirstLetter(){
        int num = 0;
        for (int i = 1; i < letters.length; i++) {
           if (letters[i] == letters[0]) {
               letters[i] = '0';
           }
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < letters.length; i ++) {
            if (letters[i] != '0') {
                stringBuilder.append(letters[i]);
            }

        }
        newWord = stringBuilder.toString();
    }

    @Override
    public String toString() {
        return "Word{" +
                "word='" + word + '\'' +
                '}';
    }
}
