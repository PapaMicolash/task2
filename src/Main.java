import java.net.ServerSocket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * public class Main serves for work with text, contains objects of classes: Text, Sentence and Word
 * also contains decisions for the main tasks
 * @author Nikolay Sizykh
 * @version 1.3
 */

public class Main {

    public static void main(String[] args) {

        System.out.println("Reading text from file");

        Reader reader = new Reader();

        System.out.println("Creation of the Text class and work with its methods");

        Text text = new Text(reader.getNextString());
        int numSentences = reader.getNumString();
        text.setNumSentence(numSentences);
        text.setSentences();

        System.out.println(text.toString());

        System.out.println("Receive sentences");

        String[] sentences = text.getSentences();

        for (int i = 0; i < sentences.length; i++) {
            System.out.println(i + 1 + ". " + sentences[i]);
        }

        System.out.println("==========First task===========");

        Sentence[] interrogativeSentences = new Sentence[text.findInterrogativeSentence().length];

        for (int i = 0; i < interrogativeSentences.length; i++) {
            interrogativeSentences[i] = new Sentence(text.findInterrogativeSentence()[i]);
            interrogativeSentences[i].setAllSentence();
            interrogativeSentences[i].setWords();
            interrogativeSentences[i].setPunctuationMarks();
            for (int j = 0; j < interrogativeSentences[i].getWords().length; j++) {
                Word questionWord = new Word(interrogativeSentences[i].getWords()[j]);
                questionWord.setLetters();
                questionWord.eraseNextFirstLetter();
                interrogativeSentences[i].replaceWord(questionWord, questionWord.getNewWord());
            }
            interrogativeSentences[i].setNewSentence();
            text.replaceSentence(interrogativeSentences[i], interrogativeSentences[i].getNewSentence());
        }

        for (int i = 0; i < text.getSentences().length; i++) {
            System.out.println(text.getSentences()[i]);
        }

        System.out.println("===========Second task============");
        Sentence firstSentence = new Sentence(text.getSentences()[0]);
        firstSentence.setWords();

        Sentence[] sentences1 = new Sentence[text.getSentences().length - 1];

        StringBuilder sb = new StringBuilder();
        int allLength = 0;
        for (int i = 0; i < sentences1.length; i ++) {
            sentences1[i] = new Sentence(text.getSentences()[i+1]);
            sb.append(sentences1[i].getSentence());
        }

        String loseText = sb.toString();
        Sentence textSentence = new Sentence(loseText);
        textSentence.setWords();

        String[] textWords = textSentence.getWords();

        int count = 0;
        for (int i = 0; i <firstSentence.getWords().length; i++) {
            for (int j = 0; j < textWords.length; j ++) {
                if (firstSentence.getWords()[i].equalsIgnoreCase(textWords[j])) {
                    count ++;
                }
            }
            if (count == 0) {
                System.out.println(firstSentence.getWords()[i]);
            }
            count = 0;
        }

        System.out.println("=============Third task============");

        Sentence[] exclamatorySentences = new Sentence[text.findExclamatorySentence().length];

        for (int i = 0; i < exclamatorySentences.length; i++) {
            exclamatorySentences[i] = new Sentence(text.findExclamatorySentence()[i]);
            exclamatorySentences[i].setAllSentence();
            exclamatorySentences[i].setWords();
            exclamatorySentences[i].setPunctuationMarks();
            exclamatorySentences[i].swapWords(0, exclamatorySentences[i].getWords().length-1);
            text.replaceSentence(exclamatorySentences[i], exclamatorySentences[i].getNewSentence());
        }


        for (int i = 0; i < text.getSentences().length; i ++) {
            System.out.println(text.getSentences()[i]);
        }

        System.out.println("============Fourth task=============");

        Sentence[] narrativeSentences = new Sentence[text.findNarrativeSentence().length];

        char first = 'o';
        char last = 'r';

        for (int i = 0; i < narrativeSentences.length; i++) {
            narrativeSentences[i] = new Sentence(text.findNarrativeSentence()[i]);
            narrativeSentences[i].deleteSubSentence(narrativeSentences[i].getSentence().indexOf(first), narrativeSentences[i].getSentence().lastIndexOf(last));
            text.replaceSentence(narrativeSentences[i], narrativeSentences[i].getNewSentence());
        }

        for (int i = 0; i < text.getSentences().length; i ++) {
            System.out.println(text.getSentences()[i]);
        }

    }
}
